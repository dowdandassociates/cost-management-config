CREATE TABLE aws_billing_detailed_line_items
(
id_                     CHARACTER VARYING,
account_                CHARACTER VARYING,
month_                  CHARACTER VARYING,
Invoice_I_D               CHARACTER VARYING,
Payer_Account_Id          CHARACTER VARYING,
Linked_Account_Id         CHARACTER VARYING,
Record_Type              CHARACTER VARYING,
Product_Name             CHARACTER VARYING,
Rate_Id                  CHARACTER VARYING,
Subscription_Id          CHARACTER VARYING,
Pricing_Plan_Id           CHARACTER VARYING,
Usage_Type               CHARACTER VARYING,
Operation               CHARACTER VARYING,
Availability_Zone        CHARACTER VARYING,
Reserved_Instance        BOOLEAN,
Item_Description         CHARACTER VARYING,
Usage_Start_Date          TIMESTAMP WITH TIME ZONE,
Usage_End_Date            TIMESTAMP WITH TIME ZONE,
Usage_Quantity           NUMERIC,
Rate                    NUMERIC,
Cost                    NUMERIC,
Blended_Rate            NUMERIC,
Blended_Cost            NUMERIC,
Un_Blended_Rate         NUMERIC,
Un_Blended_Cost         NUMERIC
)
;

